from extras.plugins import PluginMenuItem, PluginMenu

menu = PluginMenu(
    label='Mail Reporter',
    groups=(
        (
            '',
            (
                PluginMenuItem(
                    link="plugins:netbox_mailreporter:mailreporter_list",
                    link_text="Configurations",
                    permissions=['netbox_mailreporter.view_mailreporter'],
                ),
            )
        ),
    ),
    icon_class='mdi mdi-email-arrow-right'
)
