TABLE_STATUS = """{% if record.status == "SUCCESS" %}
                    <span class="badge bg-success">{{ record.status }}</span>
                {% elif record.status == "CREATED" %}
                    <span class="badge bg-warning">{{ record.status }}</span>
                {% elif record.status == "FAIL" %}
                    <span class="badge bg-danger">{{ record.status }}</span>
                {% elif record.status == "RUNNING" %}
                    <span class="badge bg-info">{{ record.status }}</span>
                {% elif record.status == "INITIALIZED" %}
                    <span class="badge bg-primary">{{ record.status }}</span>
                {% else %}
                    {{ record.status }}        
            {% endif %}"""

CHANGE_TABLE_STATUS = """{% if record.status == "SUCCESS" %}
                    <span class="badge bg-success">{{ record.status }}</span>
                {% elif record.status == "FAIL" %}
                    <span class="badge bg-danger">{{ record.status }}</span>
                {% elif record.status == "CREATED" and record.current_value is None %}
                    <span class="badge bg-warning">{{ record.status }}</span>
                {% elif record.new_value == record.current_value and record.last_applied is None %}
                    <span class="badge bg-info">{{ record.status }}</span>
                {% elif record.new_value != record.current_value and record.last_applied is None %}
                    <span class="badge bg-warning">{{ record.status }}</span>
                {% else %}
                    {{ record.status }}        
            {% endif %}"""


REPORT_BUTTON = """<a href="{% url 'plugins:netbox_mailreporter:mailreporter_reportchangessingle' pk=record.pk %}" type="button" class="btn btn-sm btn-indigo">
    <i class="mdi mdi-sync"></i>Report</a>"""
