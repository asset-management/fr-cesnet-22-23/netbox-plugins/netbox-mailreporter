import django_tables2 as tables
from netbox.tables import NetBoxTable
from netbox_mailreporter.models import *
from netbox_mailreporter.template_code import *


class MailReporterTable(NetBoxTable):
    name = tables.Column(
        linkify=True
    )

    status = tables.TemplateColumn(template_code=TABLE_STATUS)
    report = tables.TemplateColumn(template_code=REPORT_BUTTON)

    class Meta(NetBoxTable.Meta):
        model = MailReporter
        fields = (
            'name', 'table', 'fields', 'filters', 'email_address', 'last_sent', 'status',
            'report')
        default_columns = (
            'name', 'table', 'fields', 'filters', 'email_address', 'last_sent', 'status',
            'report')
