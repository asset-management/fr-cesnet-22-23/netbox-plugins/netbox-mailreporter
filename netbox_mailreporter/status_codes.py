FAIL = "FAIL"
SUCCESS = "SUCCESS"
RUNNING = "RUNNING"
CREATED = "CREATED"
INITIALIZED = "INITIALIZED"


def set_status(obj, stat, msg=None):
    if msg is not None:
        obj.message = msg
    obj.status = stat
    obj.save()
