# api/views.py
from netbox.api.viewsets import NetBoxModelViewSet
from .serializers import *


class MailReporterViewSet(NetBoxModelViewSet):
    queryset = MailReporter.objects.all()
    serializer_class = MailReporterSerializer
