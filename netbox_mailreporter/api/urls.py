# api/urls.py
from netbox.api.routers import NetBoxRouter
from .views import *

router = NetBoxRouter()
router.register('mailreporter', MailReporterViewSet)
urlpatterns = router.urls
