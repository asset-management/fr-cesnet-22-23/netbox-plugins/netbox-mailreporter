from django import template
from django.template import Template, Context

from netbox_mailreporter.template_code import TABLE_STATUS

register = template.Library()


@register.simple_tag
def wrap(record):
    t = Template(TABLE_STATUS)
    c = Context({"record": record})
    return t.render(c)
