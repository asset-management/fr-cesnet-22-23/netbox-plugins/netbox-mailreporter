from threading import Thread

from django.apps import apps
from django.contrib import messages
from django.core.exceptions import FieldError
from django.shortcuts import redirect
from django.utils import timezone
from netbox_mailreporter.status_codes import *
from netbox.views import generic
from netbox_mailreporter import tables, mail_reporter
from netbox_mailreporter.forms.forms import MailReporterForm
from netbox_mailreporter.models import MailReporter
from netbox_mailreporter.tables import MailReporterTable


# API CONNECTOR
class MailReporterView(generic.ObjectView):
    queryset = MailReporter.objects.all()
    template_name = 'netbox_mailreporter/mailreporter.html'
    table = MailReporterTable


class MailReporterListView(generic.ObjectListView):
    queryset = MailReporter.objects.all()
    table = tables.MailReporterTable
    template_name = 'netbox_mailreporter/mailreporter_list.html'


class MailReporterEditView(generic.ObjectEditView):
    queryset = MailReporter.objects.all()
    form = MailReporterForm
    template_name = 'generic/object_edit.html'


class MailReporterDeleteView(generic.ObjectDeleteView):
    queryset = MailReporter.objects.all()


def check_parameters(request, pk):
    reporter = MailReporter.objects.get(pk=pk)
    try:
        m = apps.get_model(reporter.table.app_label, reporter.table.model)
        objs = mail_reporter.filter_objects(m, reporter.filters)
        ret, invalid_field = mail_reporter.check_fields(m, reporter.fields)
        if not ret:
            messages.error(request, f"Field: {invalid_field} does not exist on the {m} model")
        else:
            messages.success(request, f"Fields OK, {len(objs)} objects are being monitored")
    except (ValueError, AttributeError, FieldError) as e:
        messages.error(request, f"Filter conditions are incorrect: {e}")
    return redirect(request.META['HTTP_REFERER'])


def report_changes(request):
    # changed = []
    for reporter in MailReporter.objects.all():
        mail_reporter.start_thread(reporter)
        # try:
        #     changed.extend(mail_reporter.report_single(reporter))
        #     if len(changed) == 0:
        #         messages.info(request, "No changes detected")
        #     else:
        #         messages.success(request, f"{len(changed)} changes reported")
        # except Exception as e:
        #     set_status(reporter, FAIL, f"Failed: {e}")
        #     messages.error(request, f"Failed: {e}")
    messages.info(request, "Reporters running in background")
    return redirect(request.META['HTTP_REFERER'])


def report_changes_single(request, pk):
    reporter = MailReporter.objects.get(pk=pk)
    mail_reporter.start_thread(reporter)
    messages.info(request, "Reporter running in background")
    # try:
    #     changed = mail_reporter.report_single(reporter)
    #     if len(changed) == 0:
    #         messages.info(request, "No changes detected")
    #     else:
    #         messages.success(request, f"{len(changed)} changes reported")
    # except Exception as e:
    #     set_status(reporter, FAIL, f"Failed: {e}")
    #     messages.error(request, f"Failed: {e}")
    return redirect(request.META['HTTP_REFERER'])


def init_reporter(request, pk):
    reporter = MailReporter.objects.get(pk=pk)
    reporter.last_sent = timezone.now()
    reporter.save()
    messages.success(request, "Successfully initialized")
    set_status(reporter, INITIALIZED, "")
    return redirect(request.META['HTTP_REFERER'])
