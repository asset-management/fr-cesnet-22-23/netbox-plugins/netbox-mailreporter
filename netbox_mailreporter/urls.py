from django.urls import path

from netbox.views.generic import ObjectChangeLogView, ObjectJournalView
from netbox_mailreporter import models
from netbox_mailreporter.views import *

urlpatterns = [
    path("mailreporter/", MailReporterListView.as_view(), name="mailreporter_list"),
    path("mailreporter/<int:pk>", MailReporterView.as_view(), name="mailreporter"),
    path("mailreporter/<int:pk>/delete", MailReporterDeleteView.as_view(), name="mailreporter_delete"),

    path("mailreporter/<int:pk>/edit", MailReporterEditView.as_view(), name="mailreporter_edit"),
    path('mailreporter/add', MailReporterEditView.as_view(), name='mailreporter_add'),
    path('mailreporter/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='mailreporter_changelog',
         kwargs={'model': models.MailReporter}),
    path('mailreporter/<int:pk>/journal/', ObjectJournalView.as_view(), name='mailreporter_journal',
         kwargs={'model': models.MailReporter}),
    path("mailreporter/<int:pk>/checkparams", check_parameters, name="mailreporter_checkparameters"),
    path("mailreporter/<int:pk>/reportsingle", report_changes_single, name="mailreporter_reportchangessingle"),
    path("mailreporter/<int:pk>/init", init_reporter, name="mailreporter_init"),
    path("mailreporter/report", report_changes, name="mailreporter_reportchanges"),
]
