from django.core.exceptions import ValidationError, FieldError
from django.apps import apps
from netbox.forms import NetBoxModelForm
from netbox_mailreporter import mail_reporter
from netbox_mailreporter.models import *
import re


class MailReporterForm(NetBoxModelForm):

    def clean_email_address(self):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
        addresses = self.cleaned_data["email_address"].strip(' ').split(' ')
        cleaned = []
        for addr in addresses:
            if addr.isspace() or addr is None or addr == '':
                continue
            if not re.fullmatch(regex, addr):
                raise ValidationError(f"Invalid email address \'{addr}\'")
            cleaned.append(addr)
        return ' '.join(cleaned)

    def clean_fields(self):
        table = self.cleaned_data["table"]
        fields = self.cleaned_data["fields"]
        m = apps.get_model(table.app_label, table.model)
        ret, invalid_field = mail_reporter.check_fields(m, fields)
        if not ret:
            raise ValidationError(f"Field: \'{invalid_field}\' does not exist on the {m} model")
        return fields

    def clean_filters(self):
        filters = self.cleaned_data["filters"]
        table = self.cleaned_data["table"]
        try:
            m = apps.get_model(table.app_label, table.model)
            mail_reporter.filter_objects(m, filters)
            return filters
        except Exception as e:
            raise ValidationError(f"Filter conditions are incorrect: {e}")

    class Meta:
        model = MailReporter
        fields = (
            'name', 'table', 'fields', 'filters', 'email_address',
            'email_subject', 'email_content', 'email_attachment', 'email_attachment_filename')
