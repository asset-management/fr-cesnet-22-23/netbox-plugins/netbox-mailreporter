from extras.scripts import *
from netbox_mailreporter.models import MailReporter
from netbox_mailreporter.mail_reporter import report_single
from netbox_mailreporter.status_codes import *

__version__ = "0.1.0"
__author__ = "Roman Mariancik"

# Timeout for the report run
JOB_TIMEOUT = 3600


class RunMailReporter(Script):
    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Run Mail Reporter"
        description = "Run Mail Reporter periodically"
        field_order = ["connector"]

    reporter = ObjectVar(
        model=MailReporter,
        required=True,
        label="Mail Reporter",
    )

    def run(self, data, commit):
        reporter = data['reporter']
        try:
            changed = report_single(reporter)
            return f"{len(changed)} changed objects reported!"
        except Exception as e:
            set_status(reporter, FAIL, f"Failed: {e}")
            return f"Failed: {e}"
